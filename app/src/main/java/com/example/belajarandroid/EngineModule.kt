package com.example.belajarandroid

import com.example.belajarandroid.model.Engine
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object EngineModule {
    @Singleton
    @Provides
    fun ProvidesEngine() : Engine{
        return Engine()
    }
}