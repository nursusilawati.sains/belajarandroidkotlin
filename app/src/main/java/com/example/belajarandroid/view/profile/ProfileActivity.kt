package com.example.belajarandroid.view.profile

import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.belajarandroid.R
import com.example.belajarandroid.base.BaseDialog
import com.example.belajarandroid.databinding.ActivityMainBinding
import com.example.belajarandroid.databinding.ActivityProfileBinding
import com.example.belajarandroid.view.login.LoginActivity.Companion.KEY_ADDRESS
import com.example.belajarandroid.view.login.LoginActivity.Companion.KEY_INPUT
import com.example.belajarandroid.view.login.LoginActivity.Companion.KEY_NAME

class ProfileActivity : AppCompatActivity(){

    private lateinit var binding: ActivityProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val name = intent.getStringExtra(KEY_NAME)
        val address = intent.getStringExtra(KEY_ADDRESS)
        val  valueInput = intent.getStringExtra(KEY_INPUT)

        binding.tvName.text = valueInput
        binding.tvAddress.text =address

        logout()
    }

    private fun logout(){
        binding.btnLogout.setOnClickListener {
            val dialog = BaseDialog(this,
                "Warning",
                "Are you sure logout?",
            onClicked = {
                Toast.makeText(this, "ini button Procced", Toast.LENGTH_LONG).show()
                finish()
            },
                withImage = true,
                image = R.drawable.ic_warning
            )
            dialog.setCancelable(false)
            dialog.show()
        }
    }
}