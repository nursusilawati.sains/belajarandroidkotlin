package com.example.belajarandroid.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.belajarandroid.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private lateinit var  binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)


//        var name = "John Doe"
//        Toast.makeText(applicationContext, name, Toast.LENGTH_SHORT).show()
        val names = "Susi"
        val ages = 15
        val hobbies = "makan"
        val address = "palembang"
        val genders = "female"
        val majors = "fisika"
        val univs = "sriwijaya university"
        val jobs = "BSIT Bcas"
        binding.nameku.text = names
        binding.age.text = ages.toString()
        binding.hobby.text = hobbies
        binding.addres.text = address
        binding.gender.text = genders
        binding.major.text = majors
        binding.univ.text = univs
        binding.job.text = jobs

    }
}