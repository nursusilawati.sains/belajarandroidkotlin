package com.example.belajarandroid.view.home

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.belajarandroid.R
import com.example.belajarandroid.databinding.ItemFoodCategoryBinding
import com.example.belajarandroid.model.CategoryFoodModel

class CategoryFoodAdapter() : RecyclerView.Adapter<CategoryFoodAdapter.CategoryFoodViewHolder>() {
    private var data: MutableList<CategoryFoodModel> = mutableListOf()
    private var onClickList: (CategoryFoodModel) -> Unit = {}
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryFoodViewHolder {
        return CategoryFoodViewHolder(
            ItemFoodCategoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    fun setFoodCategory(newData: MutableList<CategoryFoodModel>) {
        data=newData
        notifyDataSetChanged()
    }

    fun addOnClickFoodCategoryItem(clikCategory: (CategoryFoodModel) -> Unit) {
        onClickList = clikCategory
    }


    override fun onBindViewHolder(holder: CategoryFoodViewHolder, position: Int) {
        holder.bindingData(data[position])

    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class CategoryFoodViewHolder(val binding: ItemFoodCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindingData(data: CategoryFoodModel) {
            binding.tvCategory.text = data.title
            binding.clCategory.setOnClickListener {
                onClickList(data)
            }

            val (selectedBackgroundRes, selectedColor) = if (data.isSelected ?: false) {
                Pair(R.drawable.background_rounded_selected, Color.CYAN)
            } else {
                Pair(R.drawable.background_rounded_outline_black, Color.BLACK)
            }
            val selectedBackground =
                ContextCompat.getDrawable(binding.root.context, selectedBackgroundRes)

            binding.clCategory.background = selectedBackground
            binding.tvCategory.setTextColor(ColorStateList.valueOf(selectedColor))

        }
    }

}