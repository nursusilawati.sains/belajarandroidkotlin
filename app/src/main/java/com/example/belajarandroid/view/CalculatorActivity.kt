package com.example.belajarandroid.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.belajarandroid.databinding.ActivityCalBinding

class CalculatorActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCalBinding

    private var input1 = 0
    private var input2 = 0
    private var inputResult = 0
    private var operatorType = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCalBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val tambah = 1
        val kurang = 2
        val kali = 3
        val bagi = 4

        binding.btnTambah.setOnClickListener {
            changeOperator(operator = "+")
            operatorType = OperatorType.TAMBAH

        }

        binding.btnKurang.setOnClickListener {
            changeOperator(operator = "-")
            operatorType = OperatorType.KURANG
        }

        binding.btnKali.setOnClickListener {
            changeOperator(operator = "*")
            operatorType = OperatorType.KALI
        }

        binding.btnBagi.setOnClickListener {
            changeOperator(operator = "/")
            operatorType = OperatorType.BAGI
        }

        binding.btnResult.setOnClickListener {
            calculate()
        }
    }

    private fun calculate() {
        input1 = binding.etInput
            .text//mengambil text dari edit text
            .toString()
            .toInt()

        input2 = binding.etInput2
            .text//mengambil text dari edit text
            .toString()
            .toInt()

        when (operatorType) {
            OperatorType.TAMBAH -> {
                tambah()
            }
            OperatorType.KURANG -> {
                kurang()
            }
            OperatorType.KALI -> {
                kali()
            }
            OperatorType.BAGI -> {
                bagi()
            }
        }

        binding.tvResult.text = inputResult.toString()
    }

    private fun changeOperator(operator: String) {
        binding.tvOperator.text = operator
    }

    private fun tambah() {
        inputResult = input1 + input2
    }

    private fun kurang() {
        inputResult = input1 - input2
    }

    private fun kali() {
        inputResult = input1 * input2
    }

    private fun bagi() {
        inputResult = input1 / input2
    }
}