package com.example.belajarandroid.view.home

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.belajarandroid.R
import com.example.belajarandroid.databinding.ActivityHomeBinding
import com.example.belajarandroid.model.CategoryModel
import com.example.belajarandroid.model.NewsModel
import com.example.belajarandroid.view.detail.DetailNewsActivity

class HomeMainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding
    private val mainAdapter = HomeMainAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.componentAppBar.tvAppbar.text = "Recyle View activity"
        //binding.componentAppBar.ivBlack.visibility = View.GONE
        binding.componentAppBar.ivBlack.setOnClickListener{
            this.onBackPressed()
        }

//        val mainAdapter = HomeMainAdapter(
//            dataNews = populateData(),
//            onClickNews = { dataNews ->
//                DetailNewsActivity.navigateToActivityDetail(activity = this, dataNews = dataNews)
//            })

        binding.rvNews.adapter = mainAdapter

        val categoryAdapter = CategoryAdapter(populateDataForCategory())
        binding.rvListHorizontal.adapter = categoryAdapter

        val data = populateData()
        mainAdapter.addDataNews(data)

        mainAdapter.addOnClickNews { dataNews ->
            DetailNewsActivity.navigateToActivityDetail(activity = this, dataNews = dataNews)
        }
        binding.btnAdd.setOnClickListener {
            val newData = populateData2()
            mainAdapter.addDataNews(newData)
        }
    }

    private fun populateData2(): List<NewsModel> {
        return listOf(
            NewsModel(
                image = R.drawable.ic_hujan,
                title = "Hujan Melanda Jakarta",
                subtitle = "Jakarta Diguyur Hujan Sejak Malam, Sejumlah Wilayah di Jakbar dan Jakut Alami Genangan  Pos Angke Hulu posisi tinggi muka airnya berada pada 220 cm dengan cuaca gerimis di sekitar lokasi. Untuk Pos Sunter Hulu, tinggi muka air berada pada 150 cm dengan kondisi sekitar gerimis."
            ),
            NewsModel(
                image = R.drawable.ic_hakim,
                title = "Hakim jatuhkan vonis pidana Arif Arifin",
                subtitle = "Jakarta Diguyur Hujan Sejak Malam, Sejumlah Wilayah di Jakbar dan Jakut Alami Genangan  Pos Angke Hulu posisi tinggi muka airnya berada pada 220 cm dengan cuaca gerimis di sekitar lokasi. Untuk Pos Sunter Hulu, tinggi muka air berada pada 150 cm dengan kondisi sekitar gerimis."
            ),
            NewsModel(
                image = R.drawable.ic_ansor,
                title = "Ansor Laporkan",
                subtitle = "Jakarta Diguyur Hujan Sejak Malam, Sejumlah Wilayah di Jakbar dan Jakut Alami Genangan  Pos Angke Hulu posisi tinggi muka airnya berada pada 220 cm dengan cuaca gerimis di sekitar lokasi. Untuk Pos Sunter Hulu, tinggi muka air berada pada 150 cm dengan kondisi sekitar gerimis."
            ),
            NewsModel(
                image = R.drawable.ic_launcher_background,
                title = "Ansor Laporkan",
                subtitle = "Jakarta Diguyur Hujan Sejak Malam, Sejumlah Wilayah di Jakbar dan Jakut Alami Genangan  Pos Angke Hulu posisi tinggi muka airnya berada pada 220 cm dengan cuaca gerimis di sekitar lokasi. Untuk Pos Sunter Hulu, tinggi muka air berada pada 150 cm dengan kondisi sekitar gerimis."
            ),
            NewsModel(
                image = R.drawable.ic_launcher_background,
                title = "Ansor Laporkan",
                subtitle = "Jakarta Diguyur Hujan Sejak Malam, Sejumlah Wilayah di Jakbar dan Jakut Alami Genangan  Pos Angke Hulu posisi tinggi muka airnya berada pada 220 cm dengan cuaca gerimis di sekitar lokasi. Untuk Pos Sunter Hulu, tinggi muka air berada pada 150 cm dengan kondisi sekitar gerimis."
            )
        )
    }

    private fun populateData(): List<NewsModel> {
        val listData = listOf(
            NewsModel(
                image = R.drawable.ic_hujan,
                title = "Hujan Melanda Jakarta",
                subtitle = "Jakarta Diguyur Hujan Sejak Malam, Sejumlah Wilayah di Jakbar dan Jakut Alami Genangan  Pos Angke Hulu posisi tinggi muka airnya berada pada 220 cm dengan cuaca gerimis di sekitar lokasi. Untuk Pos Sunter Hulu, tinggi muka air berada pada 150 cm dengan kondisi sekitar gerimis."
            ),
            NewsModel(
                image = R.drawable.ic_hakim,
                title = "Hakim jatuhkan vonis pidana Arif Arifin",
                subtitle = "Jakarta Diguyur Hujan Sejak Malam, Sejumlah Wilayah di Jakbar dan Jakut Alami Genangan  Pos Angke Hulu posisi tinggi muka airnya berada pada 220 cm dengan cuaca gerimis di sekitar lokasi. Untuk Pos Sunter Hulu, tinggi muka air berada pada 150 cm dengan kondisi sekitar gerimis."
            ),
            NewsModel(
                image = R.drawable.ic_ansor,
                title = "Ansor Laporkan",
                subtitle = "Jakarta Diguyur Hujan Sejak Malam, Sejumlah Wilayah di Jakbar dan Jakut Alami Genangan  Pos Angke Hulu posisi tinggi muka airnya berada pada 220 cm dengan cuaca gerimis di sekitar lokasi. Untuk Pos Sunter Hulu, tinggi muka air berada pada 150 cm dengan kondisi sekitar gerimis."
            ),
            NewsModel(
                image = R.drawable.ic_launcher_background,
                title = "Ansor Laporkan",
                subtitle = "Jakarta Diguyur Hujan Sejak Malam, Sejumlah Wilayah di Jakbar dan Jakut Alami Genangan  Pos Angke Hulu posisi tinggi muka airnya berada pada 220 cm dengan cuaca gerimis di sekitar lokasi. Untuk Pos Sunter Hulu, tinggi muka air berada pada 150 cm dengan kondisi sekitar gerimis."
            ),
            NewsModel(
                image = R.drawable.ic_launcher_background,
                title = "Ansor Laporkan",
                subtitle = "Jakarta Diguyur Hujan Sejak Malam, Sejumlah Wilayah di Jakbar dan Jakut Alami Genangan  Pos Angke Hulu posisi tinggi muka airnya berada pada 220 cm dengan cuaca gerimis di sekitar lokasi. Untuk Pos Sunter Hulu, tinggi muka air berada pada 150 cm dengan kondisi sekitar gerimis."
            )
        )
        return listData
    }

    private fun populateDataForCategory(): List<CategoryModel> {
        val listData = listOf(
            CategoryModel(
                title = "News"
            ),
            CategoryModel(
                title = "Sport"
            ),
            CategoryModel(
                title = "Religi"
            ),
            CategoryModel(
                title = "Fashion"
            ),
            CategoryModel(
                title = "Healty"
            ),
            CategoryModel(
                title = "Teens"
            ),
            CategoryModel(
                title = "Watch"
            ),

            )
        return listData
    }
}