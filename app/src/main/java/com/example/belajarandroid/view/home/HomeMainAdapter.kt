package com.example.belajarandroid.view.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.belajarandroid.databinding.ItemNewsBinding
import com.example.belajarandroid.model.NewsModel

class HomeMainAdapter :
    RecyclerView.Adapter<HomeMainAdapter.HomeMainViewHolder> (){
    private val dataNews: MutableList<NewsModel> = mutableListOf()
    private var onClickNews: (NewsModel) -> Unit = {}

    //untuk memanggil data
    fun addDataNews(newData: List<NewsModel>){
        dataNews.addAll(newData)
        notifyDataSetChanged() //untuk memberi tahu adapter terkait perubhn
    }

    fun addOnClickNews(clickNews: (NewsModel)->Unit){
        onClickNews = clickNews
    }

    inner class HomeMainViewHolder(val binding:ItemNewsBinding):RecyclerView.ViewHolder(
        binding.root
    ){

        fun bindView(data : NewsModel, onClickNews: (NewsModel) -> Unit) {
            binding.ivItemNews.setImageResource(data.image ?: 0)
            binding.tvTitleNews.text = data.title
            binding.tvSubtitleNews2.text = data.subtitle

            binding.lvNews.setOnClickListener {
                onClickNews(data)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeMainViewHolder
    = HomeMainViewHolder(
        ItemNewsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun getItemCount(): Int = dataNews.size

    override fun onBindViewHolder(holder: HomeMainViewHolder, position: Int) {
        holder.bindView(dataNews[position], onClickNews)
    }
}