package com.example.belajarandroid.view.latihan_fragment.foodmenu

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.belajarandroid.R
import com.example.belajarandroid.databinding.FragmenFoodMenuBinding
import com.example.belajarandroid.model.CategoryFoodModel
import com.example.belajarandroid.model.FoodModel
import com.example.belajarandroid.view.home.CategoryFoodAdapter

class FoodMenuFragment : Fragment() {
    private var binding: FragmenFoodMenuBinding? = null
    private var foodAdapter = FoodAdapter()
    private var categoryFoodAdapter = CategoryFoodAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmenFoodMenuBinding.inflate(layoutInflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        foodAdapter.setData(populateData())
        binding?.rvFood?.adapter = foodAdapter
        binding?.etSearch?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                val data = populateData()
                val filterData = data.filter { data ->
                    data.title.contains(p0.toString(), true)
                            || data.category.contains(p0.toString(), true)
                }
                foodAdapter.setData(filterData.toMutableList())
                val editTextValue = binding?.etSearch?.text
                if (editTextValue?.isEmpty() == true) {
                    binding?.ivClose?.visibility = View.INVISIBLE
                } else {
                    binding?.ivClose?.visibility = View.VISIBLE
                }

            }

        })

        binding?.ivClose?.setOnClickListener {
            binding?.etSearch?.setText("")
        }

        categoryFoodAdapter.setFoodCategory(populateDataCategory())
        binding?.rvCategoryFood?.adapter = categoryFoodAdapter
        categoryFoodAdapter.addOnClickFoodCategoryItem { categoryFoodModel ->
            val categoryFoodData = populateDataCategory()

            val category = categoryFoodData.map {
                val categoryId = categoryFoodModel.id
                val isSelected = it.id == categoryId
                it.copy(isSelected = isSelected)
            }
            categoryFoodAdapter.setFoodCategory(category.toMutableList())

            val data = populateData()
            val filterData = data.filter {dataFood ->
                dataFood.category == categoryFoodModel.title
            }
            foodAdapter.setData(filterData.toMutableList())
        }
    }

    private fun populateData(): MutableList<FoodModel> {
        return mutableListOf(
            FoodModel(
                image = "https://o-cdf.sirclocdn.com/unsafe/o-cdn-cas.sirclocdn.com/parenting/images/resep-rendang-daging-sapi-.width-800.format-webp.webp",
                title = "Rendang",
                subtitle = "Rendang atau randang dalam bahasa Minangkabau (Jawi: رندڠ) adalah Masakan Minangkabau yang berbahan dasar daging yang berasal dari Sumatra Barat, Indonesia.",
                category = "pedas"
            ),

            FoodModel(
                image = "https://cdn1-production-images-kly.akamaized.net/V-8sqcc4L_JAfeik2rP4N1fLiCg=/1200x900/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/3550021/original/077027900_1629810872-IG_fenita.d2.jpg",
                title = "Tekwan",
                subtitle = "Tekwan merupakan salah satu kudapan yang umumnya terbuat dari bakso ikan dan telur. Meski begitu, cara membuat tekwan bisa dikreasikan dengan bahan lain seperti putih telur, mi instan, dan daging ayam, loh.",
                category = "Gurih"
            ),
            FoodModel(
                image = "https://o-cdn-cas.sirclocdn.com/parenting/images/" +
                        "827abeff0932d279ca199a3668bec851.width-800.format-webp.webp",
                title = "Pempek",
                subtitle = "Pempek yang merupakan makanan khas Sumatera Selatan, khususnya Palembang dibuat dari olahan tepung dan ikan.\n" +
                        "\n" +
                        "Dalam penyajiannya selalu ditemani saus berwarna cokelat kehitaman yang disebut cuko atau cuka.",
                category = "Gurih"
            ),
            FoodModel(
                image = "https://cdn1-production-images-kly.akamaized.net/VHUwNF88d1kxwKyc8NfghqWtmd4=/1280x720/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/2800387/original/035118700_1557376783-iStock-497959594.jpg",
                title = "Coklat",
                subtitle = "Makanan manis sederhana ini bukanlah kue atau camilan yang susah dibuat dan memerlukan berbagai macam bahan yang sulit didapatkan." +
                        " Dengan bahan sederhana dan langkah pembuatan yang juga mudah kamu bisa membuatnya di rumah. ",
                category = "manis"
            )
        )

    }

    private fun populateDataCategory(): MutableList<CategoryFoodModel> {
        return mutableListOf(
            CategoryFoodModel(id = 1, title = "manis", isSelected = false),
            CategoryFoodModel(id = 2, title = "Gurih", isSelected = false),
            CategoryFoodModel(id = 3, title = "pedas", isSelected = false),
            CategoryFoodModel(id = 5, title = "Asam", isSelected = false),
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}