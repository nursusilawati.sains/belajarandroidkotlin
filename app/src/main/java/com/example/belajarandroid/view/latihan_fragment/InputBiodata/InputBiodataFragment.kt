package com.example.belajarandroid.view.latihan_fragment.InputBiodata

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.belajarandroid.R
import com.example.belajarandroid.databinding.InputBiodataFragmentBinding

class InputBiodataFragment : Fragment (){

    private lateinit var binding:InputBiodataFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = InputBiodataFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnSubmit.setOnClickListener {
            handleSubmitButton()
        }
        setAppBar()
    }
    private fun setAppBar(){
        binding.layoutComponentAppbar.tvAppbar.text ="Register Profile"
        binding.layoutComponentAppbar.ivBlack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun handleSubmitButton(){
        val checketName= binding.etName.text.isNullOrEmpty().not()
        val checketEmail= binding.etEmail.text.isNullOrEmpty().not()
        val checketAddress= binding.etAddess.text.isNullOrEmpty().not()
        val checketNumber= binding.etNumberPhone.text.isNullOrEmpty().not()



        if (checketName && checketEmail && checketAddress && checketNumber){

            var bundle = Bundle().apply {
                putString(NAME_KEY, binding.etName.text.toString())
                putString(EMAIL_KEY, binding.etEmail.text.toString())
                putString(ADDRESS_KEY, binding.etAddess.text.toString())
                putString(NUMBER_KEY, binding.etNumberPhone.text.toString())
            }

            val succesFragment = SuccesBiodataFragment().apply {
                arguments = bundle
            }
            parentFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, succesFragment)
                .commit()

        } else {
            Toast.makeText(context, "Tolong Lengkapi Biodata", Toast.LENGTH_LONG).show()
        }
    }

    companion object{
        const val NAME_KEY = "name"
        const val EMAIL_KEY = "email"
        const val ADDRESS_KEY = "Address"
        const val NUMBER_KEY = "number phone"

    }
}