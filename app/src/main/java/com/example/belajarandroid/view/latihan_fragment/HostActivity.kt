package com.example.belajarandroid.view.latihan_fragment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.belajarandroid.R
import com.example.belajarandroid.databinding.ActivityHostBinding
import com.example.belajarandroid.databinding.BaseDialogBinding
import com.example.belajarandroid.view.latihan_fragment.InputBiodata.InputBiodataFragment
import com.example.belajarandroid.view.latihan_fragment.home.HomeFragment

class HostActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHostBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityHostBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initBiodataFragment()
    }

    private fun initBiodataFragment () {
      val HomeFragment = HomeFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container,HomeFragment)
            .commit()
    }
}