package com.example.belajarandroid.view.latihan_fragment.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.belajarandroid.R
import com.example.belajarandroid.databinding.HomeFragmentBinding
import com.example.belajarandroid.view.latihan_fragment.InputBiodata.InputBiodataFragment
import com.example.belajarandroid.view.latihan_fragment.foodmenu.FoodMenuFragment

class HomeFragment:  Fragment (){

    private  lateinit var binding: HomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HomeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnRegister.setOnClickListener {
            navigateToRegister()
        }
        binding.btnMenu.setOnClickListener {
            navigateToFoodMenu()
        }
        setAppBar()
    }
    private fun setAppBar(){
        //untuk layout yangpertama tidak butuh tombol back
        binding.componentAppBar.ivBlack.isVisible = false
    }

    private fun navigateToRegister(){
        val registerFragment = InputBiodataFragment()
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, registerFragment)
            .addToBackStack(REGISTER_FRAGMENT_KEY)
            .commit()
    }

    private fun navigateToFoodMenu(){
        val foodMenurFragment = FoodMenuFragment()
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, foodMenurFragment)
            .addToBackStack(FOOD_MENU_KEY)
            .commit()
    }

    companion object {
        const val REGISTER_FRAGMENT_KEY = "register"
        const val FOOD_MENU_KEY = "food menu"
    }
}