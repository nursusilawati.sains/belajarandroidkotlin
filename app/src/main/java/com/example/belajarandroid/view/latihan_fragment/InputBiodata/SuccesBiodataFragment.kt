package com.example.belajarandroid.view.latihan_fragment.InputBiodata

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.belajarandroid.databinding.InputBiodataFragmentBinding
import com.example.belajarandroid.databinding.SuccesRegisterFragmentBinding
import com.example.belajarandroid.view.latihan_fragment.InputBiodata.InputBiodataFragment.Companion.NAME_KEY

class SuccesBiodataFragment: Fragment() {

    private lateinit var binding: SuccesRegisterFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SuccesRegisterFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val getNameFromArgument = arguments?.getString(NAME_KEY)
        binding.tvName.text = getNameFromArgument
    }

}