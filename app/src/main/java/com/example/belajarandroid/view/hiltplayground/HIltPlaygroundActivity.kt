package com.example.belajarandroid.view.hiltplayground

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.belajarandroid.R
import com.example.belajarandroid.base.PreferencesHelper
import com.example.belajarandroid.databinding.ActivityHiltPlaygroundBinding
import com.example.belajarandroid.model.Engine
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint

class HIltPlaygroundActivity : AppCompatActivity() {

    private var _binding:ActivityHiltPlaygroundBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var preferenceHelper: PreferencesHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityHiltPlaygroundBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        updateValueLayout()
        binding.btnEditProfile.setOnClickListener {
            val intent = Intent(this, ProfileOverviewActivity::class.java)
            startActivity(intent)
        }
        binding.btnClearPref.setOnClickListener {
            preferenceHelper.clearDatePref()
            updateValueLayout()
        }


//        engine.startEngine()
//        val engine = Engine()
//        engine.startEngine()




    }
    private fun updateValueLayout(){
        val getNameFromLocal = preferenceHelper.getName()
        val getAddressFromLocal = preferenceHelper.getAddress()

        binding.tvNama.text= getNameFromLocal
        binding.tvInputAddress.text = getAddressFromLocal
    }

}