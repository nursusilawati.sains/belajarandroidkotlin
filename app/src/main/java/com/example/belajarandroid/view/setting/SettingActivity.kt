package com.example.belajarandroid.view.setting

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.belajarandroid.databinding.ActivityListBinding

class SettingActivity : AppCompatActivity(){

    private  lateinit var binding: ActivityListBinding

    private  val settings = listOf(
        "Profile", "Wallet", "Password", "Email", "Username", "Image Profile", "Full name", "Logout"
    )
    private val settingAdapter = SettingAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.rvSimple.layoutManager = LinearLayoutManager(this)
        binding.rvSimple.adapter = settingAdapter

        settingAdapter.addItems(settings)


    }
}