package com.example.belajarandroid.view.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.belajarandroid.databinding.ItemCategoryBinding
import com.example.belajarandroid.model.CategoryModel

class CategoryAdapter (
    private val data: List<CategoryModel>
        ): RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(
            ItemCategoryBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false)
        )
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
       holder.bindingData(data[position])

    }
    override fun getItemCount(): Int {
        return data.size
    }

    inner class CategoryViewHolder(val binding: ItemCategoryBinding):
        RecyclerView.ViewHolder(binding.root){
        fun bindingData(data: CategoryModel){
            binding.tvTitleNewsh.text = data.title

        }
    }

}