package com.example.belajarandroid.view.latihan_fragment.foodmenu

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.belajarandroid.databinding.ItemFoodBinding
import com.example.belajarandroid.model.FoodModel

class FoodAdapter() :
    RecyclerView.Adapter<FoodAdapter.FoodViewHolder> (){
    private var dataFood: MutableList<FoodModel> = mutableListOf()
//    private var onclickFood: (FoodModel) -> Unit = {}

    //untuk memanggil data
    fun setData(newData: MutableList<FoodModel>){
        dataFood = newData
        notifyDataSetChanged() //untuk memberi tahu adapter terkait perubhn
    }

//    fun addOnclickFood(clickFood: (FoodModel)->Unit){
//        onclickFood = clickFood
//    }

    inner class FoodViewHolder(val binding:ItemFoodBinding):RecyclerView.ViewHolder(
        binding.root
    ){

        fun bindView(data : FoodModel) {
//            binding.ivItemFood.setImageResource(data.image ?: 0)

            Glide.with(binding.root.context)
                .load(data.image)
                .into(binding.ivItemFood)
            binding.tvTitleFood.text = data.title
            binding.tvSubtitleFood.text = data.subtitle

//            binding.lvFood.setOnClickListener{
//                onclickFood(data)
//            }
            
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder
    = FoodViewHolder(
        ItemFoodBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun getItemCount(): Int = dataFood.size

    override fun onBindViewHolder(holder: FoodViewHolder, position: Int) {
        holder.bindView(dataFood[position])
    }
}