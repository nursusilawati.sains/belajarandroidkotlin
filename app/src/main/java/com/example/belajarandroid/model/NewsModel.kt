package com.example.belajarandroid.model

import android.icu.text.CaseMap.Title
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NewsModel (
    val image: Int?,
    val title: String?,
    val subtitle : String
    ) : Parcelable
