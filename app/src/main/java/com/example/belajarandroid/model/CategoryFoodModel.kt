package com.example.belajarandroid.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CategoryFoodModel(
    val id: Int?,
    val title:String?,
    val isSelected: Boolean?
): Parcelable