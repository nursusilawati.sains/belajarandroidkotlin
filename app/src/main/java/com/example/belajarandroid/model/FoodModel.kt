package com.example.belajarandroid.model

import android.icu.text.CaseMap.Title
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FoodModel (
    val image: String?,
    val title: String,
    val subtitle : String?,
    val category: String
    ) : Parcelable
