package com.example.belajarandroid.base

import android.app.Dialog
import android.content.Context
import android.media.Image
import android.os.Bundle
import androidx.core.view.isVisible
import com.example.belajarandroid.R
import com.example.belajarandroid.databinding.BaseDialogBinding

class BaseDialog(context: Context,
                 val title: String,
                 val subtitle: String,
                 private val onClicked: (()-> Unit),
                 private val withImage: Boolean,
                 private val image: Int? = null
                 ) : Dialog(context) {

    private  lateinit var binding : BaseDialogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = BaseDialogBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivDialog.setImageResource(image ?: R.drawable.ic_warning)

        binding.ivDialog.isVisible = withImage

        binding.tvDialogTitle.text = title
        binding.tvDialogSubtitle.text = subtitle

        binding.btnCancel.setOnClickListener { dismissdialog() }
        binding.btnProcced.setOnClickListener { onClicked.invoke() }
    }
    private fun dismissdialog(){
        dismiss()
    }
}