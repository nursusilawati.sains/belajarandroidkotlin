package com.example.belajarandroid.base

import android.app.Application
import android.content.SharedPreferences

interface SharedPreferenceModule {
    fun provideSharedPreferences(application: Application): SharedPreferences
}